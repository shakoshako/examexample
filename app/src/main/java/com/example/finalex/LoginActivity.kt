package com.example.finalex

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView

class LoginActivity : AppCompatActivity() {

    private lateinit var recyclerView: RecyclerView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        recyclerView = findViewById(R.id.recyclerView)
        recyclerView.layoutManager = LinearLayoutManager(this)
        recyclerView.adapter = RecyclerViewItemAdapter(getItemData())
    }


    private fun getItemData() : List<Item> {

        val itemList = ArrayList<Item>()


        itemList.add(
            Item(
                1,
                "",
                "",
                "",
                "",
            )
        )

        return itemList


    }


}