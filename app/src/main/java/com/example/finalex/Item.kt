package com.example.finalex

data class Item(
    var id: Int,
    var imageUrl: String,
    val title: String,
    val description: String,
    val contact: String,
)
