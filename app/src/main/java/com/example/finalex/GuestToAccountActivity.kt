package com.example.finalex

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button

class GuestToAccountActivity : AppCompatActivity() {

    private lateinit var buttonRegister: Button

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_guest_to_account)

        init()

        registerListeners()
    }
    private fun init(){

        buttonRegister = findViewById(R.id.buttonRegister)
    }

    private fun registerListeners(){

        buttonRegister.setOnClickListener {
            startActivity(Intent(this, RegisterActivity::class.java))
        }
    }
}