package com.example.finalex

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import com.google.firebase.auth.FirebaseAuth

class ChangePasswordActivity : AppCompatActivity() {

    private lateinit var editTextPassword: EditText
    private lateinit var editTextRepeatPassword: EditText
    private lateinit var buttonChangePassword: Button

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_change_password)


        init()

        registerListeners()

    }

    private fun init(){

        editTextPassword = findViewById(R.id.editTextPassword)
        editTextRepeatPassword = findViewById(R.id.editTextRepeatPassword)
        buttonChangePassword = findViewById(R.id.buttonChangePassword)

    }

    private fun registerListeners(){

        buttonChangePassword.setOnClickListener {

            val newPassword = editTextPassword.text.toString()
            val newPasswordRepeat = editTextRepeatPassword.text.toString()

            if(newPassword.isEmpty() || newPasswordRepeat.isEmpty()){
                Toast.makeText(this, "ცარიელია.", Toast.LENGTH_SHORT).show()
                return@setOnClickListener
            }

            if (newPassword.length < 8) {
                Toast.makeText(this, "პაროლი უნდა შეიცავდეს მინიმუმ 8 სიმბოლოს!", Toast.LENGTH_SHORT).show()
                return@setOnClickListener
            }

            if (newPassword != newPasswordRepeat){
                Toast.makeText(this, "პაროლები არ ემთხვევა!", Toast.LENGTH_SHORT).show()
                return@setOnClickListener
            }

            FirebaseAuth.getInstance().currentUser?.updatePassword(newPassword)
                ?.addOnCompleteListener { task ->
                    if(task.isSuccessful){
                        Toast.makeText(this, "პაროლი წარმატებით შეიცვალა!", Toast.LENGTH_SHORT).show()
                    }else{
                        Toast.makeText(this, "Error!", Toast.LENGTH_SHORT).show()
                    }
                }
        }
    }
}
